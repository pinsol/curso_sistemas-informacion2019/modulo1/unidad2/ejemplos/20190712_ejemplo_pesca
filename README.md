Para utilizar y/o comprender este proyecto.

1. El caso del supuesto cliente se puede leer en el enunciado con formato pdf.
2. Para crear la BBDD existe un par de scripts que se ejecutará
   en el siguiente orden:
   
        a) 20190710_pesca.sql           <- Estructura
        b) 20190710_pesca_GEN.sql       <- Datos usados
        
3. Para facilitar la comprensión de la base datos echa un vistazo al diagrama.

 - 20190710_pesca.sql.dia    <- necesario el software dia para su visualización
 - PESCA_databasediagram.png <- puede visionalizarlo desde le mismo navegador
   
4. Comprobar una serie de Consultas sobre la base de datos con el archivo sql.
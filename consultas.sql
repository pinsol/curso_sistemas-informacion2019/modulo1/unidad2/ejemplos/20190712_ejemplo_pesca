﻿/**
                                                        MÓDULO I - UNIDAD 2 - EJEMPLO DE PESCA
**/
USE pesca;

/* 1. Indica el nombre de los clubes que tienen pescadores */
SELECT
  c.nombre
FROM pescadores p
  JOIN clubes c
    ON p.club_cif = c.cif
;

/* 2. Indica el nombre de los clubes que no tienen pescadores */
SELECT
  c.nombre
FROM clubes c
  LEFT JOIN pescadores p
    ON c.cif = p.club_cif
WHERE p.club_cif IS NULL
;

/* 3. Indica el nombre de los cotos autorizados a algun club */
SELECT DISTINCT
  a.coto_nombre
FROM autorizados a
;

/* 4. Indica la provincia que tiene cotos autorizados a algun club */
SELECT
  c.provincia
FROM autorizados a
  JOIN cotos c
    ON a.coto_nombre = c.nombre
;

/* 5. Indica el nombre de los cotos que no están autorizados a ningun club */
SELECT
  c.nombre
FROM
  cotos c
  LEFT JOIN autorizados a ON c.nombre = a.coto_nombre
WHERE a.coto_nombre IS NULL
;

/* 6. Indica el número de ríos por provincia con cotos */
SELECT
  c.provincia,
  COUNT(DISTINCT rio) nRios
FROM
  cotos c
    GROUP BY c.provincia
;

  -- con subconsulta
  SELECT c1.provincia,COUNT(*) nRios FROM 
  (SELECT
    c.provincia,c.rio 
   FROM
    cotos c
  ) c1
     GROUP BY c1.provincia
  ;
/* 7. Indica el número de ríos por provincia con cotos autorizados */
SELECT
  c.provincia,
  COUNT(DISTINCT rio) nRios
FROM autorizados a
  JOIN cotos c ON a.coto_nombre = c.nombre
    GROUP BY c.provincia
;

/* 8. Indica el nombre de la provincia con más cotos autorizados */
SELECT
  c3.provincia 
FROM
  (SELECT MAX(c1.ncotos) maximo FROM (SELECT c.provincia,COUNT(DISTINCT c.nombre) ncotos FROM autorizados a JOIN cotos c ON a.coto_nombre = c.nombre GROUP BY c.provincia) c1) c2
    JOIN (SELECT c.provincia,COUNT(DISTINCT c.nombre) ncotos FROM autorizados a JOIN cotos c ON a.coto_nombre = c.nombre GROUP BY c.provincia) c3 ON c2.maximo=c3.ncotos
;

/* 9. Indica el nombre del pescador y el nombre de su ahijado */
SELECT
  p1.nombre pescador,
  p.nombre ahijado
FROM
  apadrinar a
  JOIN pescadores p ON a.ahijado = p.numSocio
  JOIN pescadores p1 ON a.padrino = p1.numSocio
;

/* 10. Indica el número de ahijados de cada pescador */
SELECT
  a.padrino,
  COUNT(DISTINCT a.ahijado) nAhijados
FROM
  apadrinar a
    GROUP BY a.padrino
;